#ifndef BMP_IMAGE_ROTATOR_BMP_H
#define BMP_IMAGE_ROTATOR_BMP_H

#include "../image/image.h"
#include "stdint.h"
#include "stdio.h"


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_STREAM
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

extern const char *read_status_msgs[];

enum read_status from_bmp (FILE* in, struct image* img);
enum write_status to_bmp (FILE* out, struct image const* img);

#endif //BMP_IMAGE_ROTATOR_BMP_H

