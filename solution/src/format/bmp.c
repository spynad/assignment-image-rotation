#include "bmp.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfSignature;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

const char *read_status_msgs[5] = {"", "invalid signature\n", "invalid bitmap\n", "invalid header\n", "invalid stream\n"};

static size_t bmp_calc_padding(const struct image img) {
    return ((img.width * 3) % 4) ? (4 - (img.width * 3) % 4) : 0;
}

static enum read_status bmp_check_header (const struct bmp_header header) {
    unsigned char bytes[2];
    bytes[0] = header.bfSignature & 0xFF;
    bytes[1] = (header.bfSignature >> 8) & 0xFF;
    if (!(bytes[0] == 'B' && bytes[1] == 'M')) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_HEADER;
    }
    //debug_print("Invalid signature: %c%c", bytes[0], bytes[1]);
    return READ_OK;
}

static bool bmp_to_image (FILE* file, const struct bmp_header* header, struct image* img) {
    *img = image_init(header->biWidth, header->biHeight);
    const size_t padding = bmp_calc_padding(*img);
    for (int32_t i = 0; i < header->biHeight; ++i) {
	    //marray->array->c array of pixels
	    if (!fread(img->data.data + i * img->width, sizeof (struct pixel), header->biWidth, file)) {
            image_destroy(img);
            return false;
        }
        if (fseek(file, padding, SEEK_CUR)) {
            image_destroy(img);
            return false;
        }
    }

    return true;
}

static uint32_t bmp_calc_file_size(const struct image img) {
    return sizeof (struct bmp_header)
    + 3 * sizeof (uint8_t) * (img.height * img.width)
    + ((img.width % 4) ? (img.height + 1) * (4 - (img.width % 4)) : 0);
}

static struct bmp_header bmp_construct_header (struct image const* img) {
    struct bmp_header header = {0};
    const uint32_t fileSize   = bmp_calc_file_size(*img);
    header.bfSignature  = ('M' << 8) + 'B';
    header.bOffBits     = 54;
    header.biSize       = 40;
    header.biWidth      = img->width;
    header.biHeight     = img->height;
    header.biPlanes     = 1;
    header.biBitCount   = 24;
    header.bfileSize    = fileSize;
    header.biSizeImage  = fileSize - sizeof (struct bmp_header);

    return header;
}

enum read_status from_bmp (FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (in) {
        if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
            //debug_print("Failed to read bmp header from file &p", in);
            return READ_INVALID_HEADER;
        }

        enum read_status status = bmp_check_header(header);
        if (status != READ_OK) {
            return status;
        }

        if (!bmp_to_image(in, &header, img)) {
            return READ_INVALID_BITS;
        }
    } else return READ_INVALID_STREAM;

    return READ_OK;
}

enum write_status to_bmp (FILE* out, struct image const* img) {
    struct bmp_header header = bmp_construct_header(img);
    size_t padding = bmp_calc_padding(*img);

    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) {
        //debug_print("Unable to write a bmp header to a file");
        return WRITE_ERROR;
    }

    for (int32_t i = 0; i < header.biHeight; ++i) {

        if (!fwrite(img->data.data + i * img->width, sizeof (struct pixel), header.biWidth, out)) {
            return WRITE_ERROR;
        }
        const int32_t zero = 0;
        if (!fwrite(&zero, padding, 1, out)) return WRITE_ERROR;

    }

    return WRITE_OK;
}

