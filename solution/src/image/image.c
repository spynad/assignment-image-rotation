#include "image.h"
#include "malloc.h"

struct image image_init (int32_t width, int32_t height) {
    //debug_print("Allocating memory for image with size %" PRIi32 "x%" PRIi32 "\n", width, height);
    struct image image = {0};
    //debug_print("Allocated successfully at %p\n", image);
    image.width  = width;
    image.height = height;
    struct optional_array_pixel opt_array = array_pixel_init(height * width);
    if (opt_array.valid) {
        image.data = opt_array.value;
        return image;
    }
    //debug_print("Malloc failure");

    return image;
}

enum image_status image_get_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel) {
    struct optional_pixel opt_pixel = array_pixel_get(image->data, x * y);
    if (opt_pixel.valid) {
        *pixel = opt_pixel.value;
        return IMAGE_OK;
    } else {
        //debug_print("Invalid coordinates: x:%" PRIi32 " y:%" PRIi32 "\n");
        return IMAGE_INVALID_COORDINATES;
    }
}

enum image_status image_set_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel) {
    if (array_pixel_set(image->data, x * y, *pixel)) {
        return IMAGE_OK;
    } else {
        //debug_print("Invalid coordinates: x:%" PRIi32 " y:%" PRIi32 "\n");
        return IMAGE_INVALID_COORDINATES;
    }
}

void image_destroy (struct image* image) {
    //debug_print("Deallocating image at address %p...\n", image);
    array_pixel_free(image->data);
}
