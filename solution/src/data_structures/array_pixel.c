#include "array_pixel.h"
#include "../image/pixel.h"
#include "malloc.h"
#include "stddef.h"

const struct optional_array_pixel NONE_ARRAY_PIXEL = {0};

struct optional_array_pixel array_pixel_init(size_t size) {
    if (size > 0) {
        struct pixel* array = malloc(sizeof(struct pixel) * size);
        if (array) {
            struct array_pixel array_pixel = {.data = array, .size = size};
            array_pixel_zero(array_pixel);
            return some_array_pixel(array_pixel);
        }
    }

    return NONE_ARRAY_PIXEL;
}

static void array_pixel_zero(struct array_pixel a) {
    for (size_t i = 0; i < a.size; i++) {
        a.data[i] = PIXEL_WHITE;
    }
}

struct optional_pixel array_pixel_get(struct array_pixel a, size_t i) {
    if (i > a.size) {
        return NONE_PIXEL;
    }

    return some_pixel(a.data[i]);
}

bool array_pixel_set(struct array_pixel a, size_t i, struct pixel value) {
    if (i > a.size) {
        return false;
    }
    a.data[i] = value;
    return true;
}

void array_pixel_free(struct array_pixel a) {
    if ( a.size > 0 ) {
        free(a.data);
    }
}

struct optional_array_pixel some_array_pixel(struct array_pixel array) {
    return (struct optional_array_pixel) {.value = array, .valid = true};
}
