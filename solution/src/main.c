#include <stdio.h>

#include "format/bmp.h"
#include "file/fileio.h"
#include "transformation/transform_rotate.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: ./image_transformer <source_file> <destination_file>\n");
        return 1;
    }

    FILE *in = NULL;
    FILE *out = NULL;

    struct image new_image = {0};
    if (open_file(argv[1], &in, FILE_RB)) {
        struct image image = {0};
        enum read_status status = from_bmp(in, &image);
        if (status == READ_OK) {
            new_image = rotate(image);
        } else {
            image_destroy(&image);
            close_file(&in);
            fprintf(stderr, "error: ");
            fprintf(stderr, "%s", read_status_msgs[status]);
            return 1;
        }
        image_destroy(&image);
        close_file(&in);
    } else return 1;

    if (open_file(argv[2], &out, FILE_WB)) {
        if (to_bmp(out, &new_image) != WRITE_OK) {
            fprintf(stderr, "error: unable to write a file\n");
        }
        image_destroy(&new_image);
        close_file(&out);
    } else return 1;

    return 0;
}
