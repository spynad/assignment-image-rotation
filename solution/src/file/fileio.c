#include "fileio.h"

bool open_file (const char* filename, FILE** file, const char* mode) {
    if (!filename) return false;
    *file = fopen( filename, mode );
    if (!*file) return false;

    return true;
}

bool close_file (FILE** file) {
    if (*file) {
        if (fclose(*file)) {
            //debug_print("Failed to close file %p", *file);
            return false;
        }
        return true;
    }
    //debug_print("Tried to close null file");
    return false;
}
