#include "transform_rotate.h"

struct image rotate(struct image const source ) {
    struct image new_image = image_init(source.height, source.width);

    for (size_t y = 0; y < source.height; ++y) {
        for (size_t x = 0; x < source.width; ++x) {
            *(new_image.data.data + x * source.height + (source.height - 1 - y)) = *(source.data.data + y * source.width + x);
        }
    }

    return new_image;
}
